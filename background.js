var reviews = []; // In memory cache for the user's entire doclist.
var reviewsToReview = []; // reviews you have to review
var reviewsToSummarize = []; // reviews you have to summarize
var reviewsOutForReview = []; // review the user created
var reviewsCompleted = [];
var notifications_count = 0; // count of notifications

var refreshRate = 60 * 5; // 5 min default.
var pollIntervalMin = 1000 * refreshRate;
var requests = [];
var util = {};

var poller = {};
var pollIntervalMax = 1000 * 60 * 60;  // 1 hour
var requestFailureCount = 0;  // used for exponential backoff
var requestTimeout = 1000 * 5;  // 5 seconds
var crucible_url = localStorage.crucible_url || null; // Crucible URL
var crucible_auth = localStorage.crucible_auth || null; // Crucible AUTH (get it using the rest api)

/**
* Sets up a future poll for the user's document list.
*/
util.scheduleRequest = function() {
	var exponent = Math.pow(2, requestFailureCount);
	var delay = Math.min(pollIntervalMin * exponent,
		pollIntervalMax);
		delay = Math.round(delay);

	var req = window.setTimeout(function() {
		poller.getReviewsList('toReview');
		poller.getReviewsList('toSummarize');
		poller.getReviewsList('outForReview');
		poller.getReviewsList('completed/details');
		util.scheduleRequest();
	}, delay);
	requests.push(req);

};




// Review object
poller.Review = function(entry) {
	this.entry = entry;
	this.name = entry.name;
	this.description = entry.description;
	this.link = crucible_url + '/cru/' + entry.permaId.id;
	this.id = entry.permaId.id;
	this.dueDate = entry.dueDate;
	
	this.unread = 0;
	if (entry.stats) {
		for (var i=0, user; user = entry.stats[i]; ++i) {
			this.unread += user.unread + user.leaveUnread;
		}
	}
};

/**
* Fetches the list of reviews
* type: type of review that you have to add
*/
poller.getReviewsList = function(review_type) {

	clearPendingRequests();
	
	// URL to use for the request
	url = crucible_url + '/rest-service/reviews-v1/filter/' + review_type + '.json?FEAUTH=' + crucible_auth;
	
	// xhr Request
	var xhr = new XMLHttpRequest();
	xhr.open("GET", url, true);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			// JSON.parse does not evaluate the attacker's scripts.
			var resp = xhr.responseText;
			poller.processReviewsListResults(resp, xhr, review_type);
		}
	}
	xhr.send();

};

poller.processReviewsListResults = function(response, xhr, review_type) {
	switch(review_type){
		case 'toReview':
			reviewsToReview	= [];
			break;
		case 'toSummarize':
			reviewsToSummarize = [];
			break;
		case 'outForReview':
			reviewsOutForReview = [];
			break
		case 'completed/details':
			reviewsCompleted = [];
			break
	}
	//reviews = [];
	if (xhr.status != 200) {
		
		console.log ('an error occured - you either have a misconfiguration or the server is not responding');
		//handleError(xhr, response);
		setIcon({'text': 'E!'});
		return;
	} else {
		requestFailureCount = 0;
	}

	var data = JSON.parse(response);
	
	switch(review_type){
		case 'toReview':
		case 'toSummarize':
		case 'outForReview':
			for (var i = 0, entry; entry = data.reviewData[i]; ++i) {
				switch(review_type){
					case 'toReview':
						reviewsToReview.push(new poller.Review(entry));
						break;
					case 'toSummarize':
						reviewsToSummarize.push(new poller.Review(entry));
						break;
					case 'outForReview':
						reviewsOutForReview.push(new poller.Review(entry));
						break
				}
			}
			break
		case 'completed/details':
			for (var i = 0, entry; entry = data.detailedReviewData[i]; ++i) {
				switch(review_type){
					case 'completed/details':
						var thisReview = new poller.Review(entry);
						if (thisReview.unread>0) {
							// If it has no comments, don't bother
							reviewsCompleted.push(thisReview);
						}
						break
				}
			}
			break
	}
	// display the number of reviews in the icon
	updateNotifications();
	
};

function updateNotifications(){
	
	notifications_count = reviewsToReview.length + reviewsToSummarize.length + reviewsCompleted.length;
	if (notifications_count > 0)
		setIcon({'text': notifications_count.toString()});
	else
		setIcon({'text': ''});
}

function setIcon(opt_badgeObj) {
	if (opt_badgeObj) {
		var badgeOpts = {};
		if (opt_badgeObj && opt_badgeObj.text != undefined) {
			badgeOpts['text'] = opt_badgeObj.text;
		}
		if (opt_badgeObj && opt_badgeObj.tabId) {
			badgeOpts['tabId'] = opt_badgeObj.tabId;
		}
		chrome.browserAction.setBadgeText(badgeOpts);
	}
};

function clearPendingRequests() {
	for (var i = 0, req; req = requests[i]; ++i) {
		window.clearTimeout(req);
	}
	requests = [];
};

// Load the reviews
window.onload = function(){

	poller.getReviewsList('toReview');
	poller.getReviewsList('toSummarize');
	poller.getReviewsList('outForReview');
	poller.getReviewsList('completed/details');
	util.scheduleRequest();
}